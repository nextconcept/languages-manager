var manager = function(defLang){
  this.defLang = defLang;
  this.Return = function(json, lang){
    return this.Loop(json, lang);
  }
  this.Loop = function(json, lang){
    if(typeof json == "object"){
      if(Object.keys(json).length > 0){
        var keys = Object.keys(json);
        var returnBack = json;
        if(this.includes(keys, this.defLang) || this.includes(keys, lang)){
          if(this.includes(keys, lang)){
            returnBack = json[lang];
          }
          else{
            returnBack = json[this.defLang];
          }
        }
        else{
          /*Erreur possible*/
          for(i = 0; i < keys.length; i++){
            var x = i;
            var jsonComing = this.Loop(returnBack[keys[i]], lang);
            i = x;
            if(typeof jsonComing == "object"){
              /*console.log("");
              console.log(i);
              console.log(keys.length);
              console.log(keys[i]);*/
              returnBack[keys[i]] = {};
              returnBack[keys[i]] = jsonComing;
            }
            else{
              /*console.log("");
              console.log(i);
              console.log(keys.length);
              console.log(keys[i]);*/
              returnBack[keys[i]] = jsonComing;
            }
          }
        }
        return returnBack;
      }
      else{
        return json
      }
    }
    else{
      return json;
    }
  }
  this.includes = function(array, string){
    var included = false;
    var i = 0;
    while(!included && i < array.length){
      if(array[i] == string){
        included = true;
      }
      i++;
    }
    return included;
  }
}
module.exports = manager;
